.PHONY: bionic-toolchain ctng-toolchain ctng-menuconfig

ifndef CT-NG_PATH
CT-NG_PATH := $(CURDIR)/../../external/crosstool-ng
endif
ifndef CTNG_KERNEL_VERSION
CTNG_KERNEL_VERSION := 2.6.29
endif
FINAL_CXX := ctng/built/bin/arm-unknown-linux-gnueabi-g++

unexport LD_LIBRARY_PATH #glibc build doesn't like this.

ctng_config_defined:
ifndef CTNG_CONFIG
	@echo CTNG_CONFIG is not set. Cannot continue. && exit 1
endif

crosstool-ng:
	@cd ../.. && make crosstool-ng

bionic-toolchain:
	@cd bionic && ./build-ndk
ctng-toolchain: $(FINAL_CXX) crosstool-ng ctng_config_defined
$(FINAL_CXX): kernel-sources ctng/$(CTNG_CONFIG)
	@cp ctng/$(CTNG_CONFIG) ctng/.config
	@cd ctng && $(CT-NG_PATH)/ct-ng build
	@chmod -R u+w ctng/built
ctng-menuconfig: crosstool-ng ctng_config_defined
	@cp ctng/$(CTNG_CONFIG) ctng/.config || exit 0
	@cd ctng && $(CT-NG_PATH)/ct-ng menuconfig
	@cp ctng/.config ctng/$(CTNG_CONFIG)
distclean:
	@cd ctng && $(CT-NG_PATH)/ct-ng distclean
	@cd ctng && rm -r built || exit 0
	@rm -r bionic/android-ndk-* || exit 0
	@rm -r bionic/downloads || exit 0
	
kernel-sources: ctng/linux-$(CTNG_KERNEL_VERSION).tar.bz2
ctng/linux-$(CTNG_KERNEL_VERSION).tar.bz2:
	@wget http://www.kernel.org/pub/linux/kernel/v2.6/linux-$(CTNG_KERNEL_VERSION).tar.bz2 -O ctng/linux-$(CTNG_KERNEL_VERSION).tar.bz2
